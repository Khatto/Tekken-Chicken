$(document).ready(function() {
  //create the array of characters

  alert("Welcome to TekkenChicken.com! This website is in alpha state. Mouse over the blue cells to see a characters attacks!)");


  var characters =
  ['Alex', 'Alisa', 'Ancient Ogre', 'Angel', 'Anna',
  'Armor King', 'Asuka', 'Baek', 'Bob', 'Christie',
  'Craig Marduk', 'Devil Jin', 'Doctor B', 'Dragonuv', 'Feng',
  'Ganryu','Heihachi', 'Hwoarang', 'Jack-6', 'Jaycee',
  'Jin', 'Jinpachi', 'Kazuya', 'King', 'Kuma', 'Kunimitsu',
  'Lars', 'Lee', 'Lei', 'Leo', 'Lili', 'Ling', 'Marshall', 'Michelle',
  'Miharu', 'Nina', 'Paul', 'P-Jack', 'Raven','Roger', 'Slim Bob',
  'Steve Fox', 'True Ogre', 'Unknown','Violet', 'Wang', 'Yoshimitsu',
  'Zafina'];

//iterate through the array
	for (i = 0; i < characters.length; i++) {
    //add the values in the array to the navicon, which will be the drop down menu.
		$('#navicon').append('<option class="'+characters[i]+'" value="#'+characters[i]+'" <a>'+characters[i]+'</a></option>');
	}
//setting a global variable to 0 named charIndex
var id = 0;
var charIndex = 0;
//when a change happens to the navicon
$('#navicon').on('change', function(event){
  //don't let the page default
    event.preventDefault();
    //using the variable i, we're gonna iterate through the characters array again
    for(i = 0; i < characters.length; i++){
      //if the value of an option that was selected matches up with a characters name
    if($("#navicon option:selected").val()== "#"+characters[i]){
      //make charIndex, the global variable THAT specific value
      charIndex = i;
   }
}
//remove the element with the id of data table
      $('#dataTable').remove();
      $('#name').remove();
      $.get( "JSON/getJSON.php?charName="+characters[charIndex], function( dataHolder ){
        data = JSON.parse(dataHolder);
        console.log("Success");
        console.log(data);
        JSON.stringify(data.attacks);
        var namVal = data.Name;
        $('#container').append('<div id="name">'+namVal+'</div>');
        $('#container').append('<div class="contentWrapper"><div id="dataTable"></div></div>');
        
        $('#dataTable').append('<div class="dataHeader"></div>');
        $('.dataHeader').append('<div class="rowStart titleRow">Command</div>');
        $('.dataHeader').append('<div class="titleRow">Orientation</div>');
        $('.dataHeader').append('<div class="titleRow">Damage</div>');
        $('.dataHeader').append('<div class="titleRow">Frames</div>');
        $('.dataHeader').append('<div class="titleRow">OnBlock</div>');
        $('.dataHeader').append('<div class="titleRow">OnHit</div>');
        $('.dataHeader').append('<div class="rowEnd titleRow">CH</div>');

        $.each( data.attacks, function(i , val) {
          var id = i;
          $('#dataTable').append('<div class="attackRow" id="' + id + '"</div>');
          $('#' + id).append('<div class="rowStart dataRow rowCommand">'+val.Command+'</div>');
          $('#' + id).append('<div class="dataRow rowOrientation">'+val.Orientation+'</div>');
          $('#' + id).append('<div class="dataRow rowDamage">'+val.Damage+'</div>');
          $('#' + id).append('<div class="dataRow rowFrames">'+val.Frames+'</div>');
          $('#' + id).append('<div class="dataRow rowOnBlock">'+val['On Block']+'</div>');
          $('#' + id).append('<div class="dataRow rowOnHit">'+val['On Hit'] +'</div>');
          $('#' + id).append('<div class="rowEnd dataRow rowCH">'+val.CH+'</div>');
        });
       
          $('div.rowOnBlock, div.rowOnHit').each(function(){
          });
          $('div.dataRow').each(function() {

          if ($(this).text() == "N/A" ) {
            $(this).addClass('NA');
          }
          });
        $('div.dataRow').each(function(){
           var frame = parseInt($(this).text(), 10);
          if (frame <=-14 ) {
            $(this).addClass('realUnsafe');
          }
          });
        $('div.rowOnBlock, div.rowOnHit').each(function(){
           var frame = parseInt($(this).text(), 10);
          if (frame >=-14 && frame <= -10 ) {
            $(this).addClass('unsafe');
          }
          });

           $.get( "JSON/gifs/requestAlbum.php?ref=" + gifRef[characters[charIndex]] , function( gifsHolder ){

            xmlDoc = $.parseXML( gifsHolder );
            $xml = $(xmlDoc);
            $attacks = $xml.find('attack');
            console.log($attacks);
            $.each( $attacks, function( i, val ) {
            $('div.rowCommand').each(function() {
              if ($(this).text() == val.firstChild.innerHTML ) {
                $(this).addClass('gifCell');
                $(this).hover(function() {
                  $('.gif').remove();
                  $(this).append('<img class="gif" src='+val.lastChild.innerHTML+'>');
                  $('.gif').slideDown('slow/400/fast', function() {
                  });
                }, function(){
                  $('.gif').slideUp('slow/400/fast', function() {
                  });
                });
              }
        });
            });
          });
            
          });
         });
});
